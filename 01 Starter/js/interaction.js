var video1;
var videoHalfWay = 0;
var formattedHalfWay = 0;

// choice parts 
var choicePart = 7
var part1 = 7;
var part2 = 20;


// question variable
var question1Asked = false
var ansChoose = false

$(document).ready(function(){
	video1 = $('#video1') // talk to the video 1 in the index.html
	// Code when DOM is ready goes here
	$('.box1').on('click', function(){
		// $.featherlight($('.persona1PopUp'))
		playPauseVideo('.popUpQuestion1'); // in the index popup 
	});
	$('.box2').on('click', function(){
		// alert("This worked!")
		playPauseVideo('.persona2PopUp')

	});	

	$('.choice1').on('click', function(){
		alert("Choice1 Clicked!")
		$.featherlight.close();
		video1[0].currentTime = part1
		ansChoose = true
	})


	$('.choice2').on('click', function(){
		alert("Choice2 Clicked!")
		$.featherlight.close();
		video1[0].currentTime = part2
		ansChoose = true
	})

	$(video1).on('loadeddata', function(){
		videoHalfWay = Math.round(this.duration/2);
		alert(videoHalfWay);
	})

	$(video1).on('timeupdate', function(){
		var currentTime = Math.round(this.currentTime)
		var durationTime = Math.round(this.currentTime)

		if(currentTime == choicePart && question1Asked == false){
			alert('this worked')
			question1Asked = true
			video1[0].pause();
			showChoiceQuestion();
		}

		if(currentTime == part2 && ansChoose == ture){
			video1[0].pause();
		}

		if(currentTime == videoHalfWay){
			// Halfway point
			alert("Video is half way!")
		}

		if (currentTime == durationNum){
			// can know user is finished the video use some analystics 
			alert('video is complete')
		}
	})

});

function playPauseVideo(popUp){
	if(video1[0].paused){
		video1[0].play();
	} else{
		video1[0].pause();
		$.featherlight($(popUp));
	}
}

function showChoiceQuestion(){
	$.featherlight($('.popUpQuestion1'))
}